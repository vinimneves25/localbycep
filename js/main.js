function setElementValue(elementId, value) {
    value == undefined ? value = "" : "";
    document.getElementById(elementId).value = value;
}

function locate(input) {
    if (input.value.length == 8) {
        let xttp = new XMLHttpRequest(),
            url = `https://viacep.com.br/ws/${input.value}/json/`;
        
        xttp.onreadystatechange = () => {
            if (xttp.readyState == 4 && xttp.status == 200) {
                let data = JSON.parse(xttp.responseText);

                console.table(data);

                setElementValue("logradouro", data["logradouro"]);
                setElementValue("complemento", data["complemento"]);
                setElementValue("bairro", data["bairro"]);
                setElementValue("localidade", data["localidade"]);
                setElementValue("uf", data["uf"]);
                setElementValue("unidade", data["unidade"]);
                setElementValue("ibge", data["ibge"])
                setElementValue("gia", data["gia"]);
            }
        };

        xttp.open("GET", url, true);
        xttp.send();
    }
}